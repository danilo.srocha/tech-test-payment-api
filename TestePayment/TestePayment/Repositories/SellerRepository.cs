﻿using Microsoft.EntityFrameworkCore;
using TestePayment.Contexts;
using TestePayment.Models;

namespace TestePayment.Repositories
{
    public class SellerRepository
    {
        private readonly PaymentContext _bd;
        public SellerRepository(PaymentContext bd) 
        {
            _bd = bd;
        }

        public void Create(Seller seller)
        {
            _bd.Sellers.Add(seller);
            _bd.SaveChanges();
        }

        public async Task<Seller> GetByIdAsync(Guid sellerId) 
        { 
            return await _bd.Sellers.FirstOrDefaultAsync(e => e.Id == sellerId);
        }

        public async Task<List<Seller>> GetAllAsync()
        {
            return await _bd.Sellers.ToListAsync();
        }
    }
}
