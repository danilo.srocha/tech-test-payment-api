﻿using Microsoft.EntityFrameworkCore;
using TestePayment.Contexts;
using TestePayment.Models;

namespace TestePayment.Repositories
{
    public class OrderRepository
    {
        private readonly PaymentContext _bd;

        public OrderRepository(PaymentContext bd)
        {
            _bd = bd;
        }
    
        public void Create(Order order)
        {
            _bd.Orders.Add(order);
            _bd.SaveChanges();
        }

        public async Task<Order> GetByIdAsync(Guid productId, Guid saleId)
        {
            return await _bd.Orders.FirstOrDefaultAsync(e => e.ProductId == productId && e.SaleId == saleId);
        }
    }
}
