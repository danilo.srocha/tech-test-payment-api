﻿using Microsoft.EntityFrameworkCore;
using TestePayment.Contexts;
using TestePayment.Models;

namespace TestePayment.Repositories
{
    public class ProductRepository
    {
        private readonly PaymentContext _bd;
        public ProductRepository(PaymentContext bd) 
        {
            _bd = bd;
        }

        public void Create(Product product)
        {
            _bd.Products.Add(product);
            _bd.SaveChanges();
        }

        public async Task<Product> GetByIdAsync(Guid productId) 
        { 
            return await _bd.Products.FirstOrDefaultAsync(e => e.Id == productId);
        }

        public async Task<List<Product>> GetAllAsync()
        {
            return await _bd.Products.ToListAsync();
        }
    }
}
