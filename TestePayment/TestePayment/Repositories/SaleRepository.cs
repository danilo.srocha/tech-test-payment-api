﻿using Microsoft.EntityFrameworkCore;
using TestePayment.Contexts;
using TestePayment.Models;

namespace TestePayment.Repositories
{
    public class SaleRepository
    {
        private readonly PaymentContext _bd;

        public SaleRepository(PaymentContext bd)
        {
            _bd = bd;
        }
    
        public void Create(Sale sale)
        {
            _bd.Sales.Add(sale);
            _bd.SaveChanges();
        }

        public async Task<Sale> GetByIdAsync(Guid saleId)
        {
            return await _bd.Sales.Include(e => e.Orders).ThenInclude(o => o.Product).Include(e => e.Seller).FirstOrDefaultAsync(e => e.Id == saleId);
        }
    }
}
