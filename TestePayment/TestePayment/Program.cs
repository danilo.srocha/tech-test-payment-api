using Microsoft.EntityFrameworkCore;
using TestePayment.Contexts;
using TestePayment.Repositories;
using TestePayment.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<PaymentContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<SellerService, SellerService>();
builder.Services.AddTransient<SellerRepository, SellerRepository>();

builder.Services.AddTransient<SaleService, SaleService>();
builder.Services.AddTransient<SaleRepository, SaleRepository>();

builder.Services.AddTransient<Orderservice, Orderservice>();
builder.Services.AddTransient<OrderRepository, OrderRepository>();

builder.Services.AddTransient<ProductService, ProductService>();
builder.Services.AddTransient<ProductRepository, ProductRepository>();

var app = builder.Build();

// Configure the HTTP Order pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
