﻿namespace TestePayment.Models
{
    public class Product : Entity
    {
        public string Name { get; set; }
        public float Price{ get; set; }
    }
}