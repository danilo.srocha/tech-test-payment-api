﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace TestePayment.Models
{
    public class Sale : Entity
    {
        public DateTime DateSale { get; set; }

        [ForeignKey(nameof(Seller))]
        public Guid SellerId { get; set; }

        public virtual Seller Seller { get; set; }

        public virtual List<Order> Orders{ get; set; }
    }
}
