﻿namespace TestePayment.Models.Dtos.Seller
{
    public class RegisterSellerDto
    {
        public RegisterSellerDto() { }

        public string Name { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
