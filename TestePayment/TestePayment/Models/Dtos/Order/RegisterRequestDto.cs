﻿namespace TestePayment.Models.Dtos.Order
{
    public class RegisterOrderDto
    {
        public RegisterOrderDto() { }

        public Guid ProductId { get; set; }
        public Guid SaleId { get; set; }
    }
}
