﻿namespace TestePayment.Models.Dtos.Sale
{
    public class RegisterSaleDto
    {
        public RegisterSaleDto() { }

        public Guid SellerId { get; set; }
        public DateTime DateSale { get; set; }
    }
}
