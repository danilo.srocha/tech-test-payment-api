﻿namespace TestePayment.Models.Dtos.Product
{
    public class RegisterProductDto
    {
        public RegisterProductDto() { }

        public string Name { get; set; }
        public float Price { get; set; }
    }
}
