﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TestePayment.Models
{
    public class Order : Entity
    {
        [ForeignKey("ProductId")]
        public Guid ProductId { get; set; }
        [ForeignKey("SaleId")]
        public Guid SaleId { get; set; }

        public virtual Product Product { get; set; }

    }
}
