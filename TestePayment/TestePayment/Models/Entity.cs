﻿using System.ComponentModel.DataAnnotations;

namespace TestePayment.Models
{
    public class Entity
    {
        [Key]
        public Guid Id { get; set; }
        public bool Active { get; set; } = true;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public void Update()
        {
            UpdatedAt = DateTime.Now;
        }

        public void Delete()
        {
            UpdatedAt = DateTime.Now;
            Active = false;
        }
    }
}
