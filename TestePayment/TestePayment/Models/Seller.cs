﻿namespace TestePayment.Models
{
    public class Seller : Entity
    {
        public string Cpf { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
