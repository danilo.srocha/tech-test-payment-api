﻿using TestePayment.Models;
using TestePayment.Models.Dtos.Seller;
using TestePayment.Repositories;

namespace TestePayment.Services
{
    public class SellerService
    {
        private readonly SellerRepository _sellerRepository;

        public SellerService(SellerRepository sellerRepository)
        {
            _sellerRepository = sellerRepository;
        }

        public Seller Register(RegisterSellerDto input)
        {
            var seller = new Seller
            {
                Cpf = input.Cpf,
                Email = input.Email,
                Name = input.Name,
                Phone = input.Phone,
            };

            _sellerRepository.Create(seller);

            return seller;
        }

        public async Task<Seller> GetByIdAsync(Guid Id)
        {
            return await _sellerRepository.GetByIdAsync(Id); ;
        }

        public async Task<List<Seller>> GetAllAsync()
        {
            return await _sellerRepository.GetAllAsync(); ;
        }
    }
}
