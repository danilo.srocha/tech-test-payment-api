﻿using TestePayment.Models;
using TestePayment.Models.Dtos.Sale;
using TestePayment.Repositories;

namespace TestePayment.Services
{
    public class SaleService
    {
        private readonly SaleRepository _saleRepository;

        public SaleService(SaleRepository saleRepository)
        {
            _saleRepository = saleRepository;
        }

        public Sale Register(RegisterSaleDto input)
        {
            var Order = new Sale
            {
                DateSale = input.DateSale,
                SellerId = input.SellerId,
            };

            _saleRepository.Create(Order);

            return Order;
        }

        public async Task<Sale> GetByIdAsync(Guid Id)
        {
            return await _saleRepository.GetByIdAsync(Id); ;
        }
    }
}
