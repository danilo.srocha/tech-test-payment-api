﻿using TestePayment.Models;
using TestePayment.Models.Dtos.Order;
using TestePayment.Repositories;

namespace TestePayment.Services
{
    public class Orderservice
    {
        private readonly OrderRepository _reqRepository;

        public Orderservice(OrderRepository reqRepository)
        {
            _reqRepository = reqRepository;
        }

        public Order Register(RegisterOrderDto input)
        {
            var order = new Order{
                ProductId = input.ProductId,
                SaleId = input.SaleId,
            };
            _reqRepository.Create(order);

            return order;
        }

        public async Task<Order> GetByIdAsync(Guid productId, Guid saleId)
        {
            return await _reqRepository.GetByIdAsync(productId, saleId); ;
        }
    }
}
