﻿using TestePayment.Models;
using TestePayment.Models.Dtos.Product;
using TestePayment.Repositories;

namespace TestePayment.Services
{
    public class ProductService
    {
        private readonly ProductRepository _productRepository;

        public ProductService(ProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Product Register(RegisterProductDto input)
        {
            var product = new Product
            {
                Name = input.Name,
                Price = input.Price,
            };

            _productRepository.Create(product);

            return product;
        }

        public async Task<Product> GetByIdAsync(Guid Id)
        {
            return await _productRepository.GetByIdAsync(Id); ;
        }

        public async Task<List<Product>> GetAllAsync()
        {
            return await _productRepository.GetAllAsync(); ;
        }
    }
}
