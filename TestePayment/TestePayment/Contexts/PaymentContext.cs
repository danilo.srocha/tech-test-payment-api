﻿using Microsoft.EntityFrameworkCore;
using TestePayment.Models;

namespace TestePayment.Contexts
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) 
        { 
        }

        public PaymentContext() : base() { }

        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<Product> Products { get; set; }

    }
}
