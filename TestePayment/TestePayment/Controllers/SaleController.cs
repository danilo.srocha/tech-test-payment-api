﻿using Microsoft.AspNetCore.Mvc;
using TestePayment.Models.Dtos.Sale;
using TestePayment.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestePayment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly SaleService _saleService;

        public SaleController(SaleService saleService)
        {
            _saleService = saleService;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<SaleController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var sale = await _saleService.GetByIdAsync(id);

            if(sale == null) return NotFound();

            return Ok(sale);
        }

        // POST api/<SellerController>
        [HttpPost]
        public IActionResult Post(RegisterSaleDto input)
        {
            _saleService.Register(input);
            return NoContent();
        }


    }
}
