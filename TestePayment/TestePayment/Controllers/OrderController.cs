﻿using Microsoft.AspNetCore.Mvc;
using TestePayment.Models.Dtos.Order;
using TestePayment.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestePayment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly Orderservice _reqService;

        public OrderController(Orderservice reqService)
        {
            _reqService = reqService;
        }

        // GET: api/<OrderController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<OrderController>/5
        [HttpGet("{productId}/{saleId}")]
        public async Task<IActionResult> Get(Guid productId, Guid saleId)
        {
            var sale = await _reqService.GetByIdAsync(productId, saleId);

            if (sale == null) return NotFound();

            return Ok(sale);
        }

        // POST api/<SellerController>
        [HttpPost]
        public IActionResult Post(RegisterOrderDto input)
        {
            _reqService.Register(input);
            return NoContent();
        }

        // PUT api/<OrderController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<OrderController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
