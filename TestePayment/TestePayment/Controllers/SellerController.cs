﻿using Microsoft.AspNetCore.Mvc;
using TestePayment.Models.Dtos.Seller;
using TestePayment.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestePayment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly SellerService _sellerService;

        public SellerController(SellerService sellerService)
        {
            _sellerService = sellerService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var sellers = await _sellerService.GetAllAsync();
            return Ok(sellers);
        }

        // GET api/<SellerController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var seller = await _sellerService.GetByIdAsync(id);

            if(seller == null) return NotFound();

            return Ok(seller);
        }

        // POST api/<SellerController>
        [HttpPost]
        public IActionResult Post(RegisterSellerDto input)
        {
            var seller = _sellerService.Register(input);
            return CreatedAtAction(nameof(Get), new { id = seller.Id }, seller);
        }

        // PUT api/<SellerController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SellerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
